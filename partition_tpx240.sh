#!/bin/bash
# Stop on error
set -e
# Stop on unitialized variables
set -u
# Stop on failed pipes
set -o pipefail

# Environment variables
SYSTEM_DISK=/dev/sda
BOOT_PARTITION=/dev/sda1
LUKS_PARTITION=/dev/sda2
TRIM=0
MOUNTPOINT="/mnt"

# Check trim
[[ -n $(hdparm -I /dev/sda | grep TRIM 2> /dev/null) ]] && TRIM=1

# Umount partitions
echo "Unmounting partitions..."
swapoff -a
umount /dev/sda1 || :
umount /dev/sda2 || :

# Partition disks
echo "Partition disks..."
sgdisk --zap-all /dev/sda
# /dev/sda1
printf "n\n1\n\n+1G\nef00\nw\ny\n" | gdisk /dev/sda
# /dev/sda2
printf "n\n2\n\n\n8e00\nw\ny\n"| gdisk /dev/sda

# Setup LUKS
echo "Setting up encryption..."
cryptsetup --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random --verify-passphrase luksFormat ${LUKS_PARTITION}
echo "Opening encrypted partition..."
cryptsetup open --type luks $([[ $TRIM -eq 1 ]] && echo "--allow-discards") ${LUKS_PARTITION} crypt

# Setup LVM
echo "Setting up LVM..."
pvcreate /dev/mapper/crypt
vgcreate lvm /dev/mapper/crypt
lvcreate --size 50G lvm -n root
lvcreate --size 8G lvm -n swap
lvcreate -l 100%FREE lvm -n home

# Format partitions
echo "Formatting disks"
# Root
echo "...Root..."
mkfs.xfs -f /dev/mapper/lvm-root
fsck /dev/mapper/lvm-root
mkdir -p $MOUNTPOINT/
mount -t xfs /dev/mapper/lvm-root $MOUNTPOINT

# Home
echo "...Home..."
mkfs.xfs -f /dev/mapper/lvm-home
fsck /dev/mapper/lvm-home
mkdir -p $MOUNTPOINT/home
mount -t xfs /dev/mapper/lvm-home $MOUNTPOINT/home

# Swap
echo "...Swap..."
mkswap /dev/mapper/lvm-swap
swapon /dev/mapper/lvm-swap

# Boot
echo "...Boot..."
mkfs.vfat -n boot /dev/sda1
mkdir -p ${MOUNTPOINT}/boot
mount /dev/sda1 ${MOUNTPOINT}/boot